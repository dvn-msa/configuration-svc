package com.dvn.msa.configurationsvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigurationSvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigurationSvcApplication.class, args);
    }

}
